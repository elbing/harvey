![HARVEY](https://harvey-os.org/images/harvey-os-logo.svg)

# Harvey: A distributed operating system

[![License GPL](https://img.shields.io/badge/license-GPL-blue.svg)](https://bitbucket.org/elbing/harvey/src/master/LICENSE)

Welcome to Harvey, we are delighted that you are interested in the
project.

This is a frozen status repo version of the official [Harvey OS project](https://harvey-os.org) for personal researching purposes.
It follows the old GPLv2 licensed version without CI, with GCC/Clang ported code, and without many [Go](https://golang.org) stuff into the original project, looking for an standalone
operating system running from disk, with bootloader and with the [APEX](https://bitbucket.org/elbing/apex) subsystem project.
Harvey OS now was rebased and its purposes and work are slightly different, so this can now considered as an abandoned and outdated try kept here for historical purposes and personal experiments.
Please, do not disturb Harvey community about this old version related problems. Instead, tell me about here.

#### This does not replace original Harvey OS in anyway.

There are also some other repos here with third party code and, eventually, incompatible licenses that couldn't be part of the official Harvey OS distribution nor even being at its [Github site](https://github.com/Harvey-OS), but all of them have instructions for setting them up into this version of Harvey.

There's an updated and local [wiki](https://bitbucket.org/elbing/harvey/wiki/) copy of the original one, with missing parts removed here intentionally and maybe others added out of scope of the original.

Enjoy it. 

##### (From the original Harvey's Readme)

## What is harvey?

Harvey is a distributed operating system. It's most directly descended from [Plan 9 from Bell Labs](https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs). This heritage spans from its distributed application architecture all the way down to much of its code. However, Harvey aims to be a more practical, general-purpose operating system, so it also uses ideas and code from other systems.

### Getting started

Start by reading the [Wiki](https://bitbucket.org/elbing/harvey/wiki/).
There's a [Getting Started](https://bitbucket.org/elbing/harvey/wiki/Getting%20Started)
guide there to learn how to get Harvey up and running and many other things.

### Docs

* [Harvey's wiki](https://bitbucket.org/elbing/harvey/wiki)
* [Apex's wiki](https://bitbucket.org/elbing/apex/wiki).

### Elsewhere

* [Slack](https://harvey-slack.herokuapp.com/)
* [Twitter](https://twitter.com/harvey_os)

## Donate to Harvey!

Harvey is a proud member of [SFC](https://sfconservancy.org/projects/current/) and gladly accepts donations.

[![Donate](https://img.shields.io/badge/Donate-PayPal-brightgreen.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7Q45ZGJBQZZVN)

