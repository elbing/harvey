/*
 * This file is part of the UCB release of Plan 9. It is subject to the license
 * terms in the LICENSE file found in the top-level directory of this
 * distribution and at http://akaros.cs.berkeley.edu/files/Plan9License. No
 * part of the UCB release of Plan 9, including this file, may be copied,
 * modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */

#include <u.h>
#include <libc.h>
#include <draw.h>
#include <memdraw.h>

/*
 * Wordaddr is deprecated.
 */
uint32_t*
wordaddr(Memimage *i, Point p)
{
	return (uint32_t*) ((uintptr_t)byteaddr(i, p) & ~(sizeof(uint32_t)-1));
}

unsigned char*
byteaddr(Memimage *i, Point p)
{
	unsigned char *a;

	a = i->data->bdata+i->zero+sizeof(uint32_t)*p.y*i->width;

	if(i->depth < 8){
		/*
		 * We need to always round down,
		 * but C rounds toward zero.
		 */
		int np;
		np = 8/i->depth;
		if(p.x < 0)
			return a+(p.x-np+1)/np;
		else
			return a+p.x/np;
	}
	else
		return a+p.x*(i->depth/8);
}
